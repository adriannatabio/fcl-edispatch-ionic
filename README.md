# Fast Cargo Logistics Electronic Dispatch

This mobile app is developed solely for Fast Cargo Logistics

## Getting Started

To run this mobile app using the surce, choose one of the following options to get started:
* [Download the latest release here](https://gitlab.com/ltbsc/fcl-edispatch-ionic/-/archive/master/fcl-edispatch-ionic-master.zip)
* Clone the repo: `git clone https://gitlab.com/ltbsc/fcl-edispatch-ionic.git`
* Fork the repo

## Project Structure

```
.
 ├── resources                    # Build files on the specific platforms (iOS, Android) and app icon + splash
 ├── src                          # This is where the app lives - *the main folder*
 ├── .editorconfig                # A helper file to define and maintain coding styles across environments
 ├── .gitignore                   # Specifies intentionally untracked files to ignore when using Git
 ├── .io-config.json              # Ionic ID
 ├── config.xml                   # Ionic config file
 ├── .ionic.config.json           # Global configuration for your Ionic app
 ├── package.json                 # Dependencies and build scripts
 ├── readme.md                    # Project description
 ├── tsconfig.json                # TypeScript configurations
 └── tslint.json                  # TypeScript linting options
```

### src directory
```
.
   ├── ...
   ├── src                       
   │   ├── app                    # This folder contains global modules and styling
   │   ├── assets                 # This folder contains images and the *data.json*
   |   ├── pages                  # Contains all the individual pages (home, tabs, category, list, single-item)
   |   ├── services               # Contains the item-api service that retrieves data from the JSON file
   |   ├── theme                  # The global SCSS variables to use throughout the app
   |   ├── declarations.d.ts      # A config file to make TypeScript objects available in intellisense
   |   ├── index.html             # The root index app file - This launches the app
   |   ├── manifest.json          # Metadata for the app
   │   └── service-worker.js      # Cache configurations
   └── ...
```


## Start the project
The project is started with the regular ionic commands.

1. Run `npm install` to install all dependencies.
2. Run `ionic serve` to start the development environment.
3. To build the project run `ionic cordova build android` or `ionic cordova build ios`. In order for you to build an iOS app, you need to run on MacOS.

## Bugs and Issues

Have a bug or an issue with this template? [Open a new issue](https://gitlab.com/ltbsc/fcl-edispatch-ionic/issues/new) here on GitLab.

## Creator

The template was created by and is maintained by **[Adrian Natabio](https://www.linkedin.com/in/adriannatabio)**