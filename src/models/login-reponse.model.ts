import { Trucker } from "./trucker.model";
import { User } from "./user.model";
import { Type } from "class-transformer";

export class LoginResponse {
  
  token: string

  @Type(() => Trucker)
  truckerInfo: Trucker

  @Type(() => User)
  userInfo: User
}