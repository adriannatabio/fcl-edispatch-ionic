export class Address {

  ID: number
  CityMunicipalityName: string
  CountryName: string
  Line1: string
  Line2: string
  PostalCode: string
  StateProvinceName: string
  
}