import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '../../app/constants';
import { plainToClass } from 'class-transformer';
import { Truck } from '../../models/truck.model';

/*
  Generated class for the TrucksProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrucksProvider {

  constructor(public http: HttpClient) {
    console.log('Hello TrucksProvider Provider');
  }

  getAllTrucks(truckerId: number) {
    let params: HttpParams = new HttpParams().set('truckerId', truckerId.toString());
    return this.http.get(API.baseURL('Trucks'), {params})
      .map(res => plainToClass(Truck, res['data'] as Object[]))
  }

}
