import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AuthConstants } from './auth.constants';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private storage: Storage) {
  }

  /**
   * Intercepts response and request
   * @param request 
   * @param next 
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.getToken().mergeMap((token: string) => {
      
      request = this.appendAuthToken(token, request);

      return next.handle(request).map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if(event.ok && typeof event.body['data'] !== 'undefined'){
            this.setToken(event.body['data']['token'])
          }
        }
        return event; 
      });
    });
  }


  /**
   * Append token to request when token is set
   * @param token 
   * @param request 
   */
  private appendAuthToken(token: string, request: HttpRequest<any>) {
    if (typeof token === 'string') {
      request = request.clone({
        setHeaders: {
          'Token': token
        }
      });
    }
    return request;
  }

  /**
   * Retrieve token from localstorage
   */
  getToken(): Observable<string> {

    const test: Promise<string> = new Promise((resolve) => {
      resolve(AuthConstants.DUMMY_SERVER_TOKEN_VALUE);
    })

    return Observable.fromPromise(test);

    // TODO: uncomment on production
    // return Observable.fromPromise(this.storage.get(AuthConstants.CLIENT_REQUEST_TOKEN));
  }


  /**
   * Store token to localstorage
   * @param token 
   */
  setToken(token: string) {
    this.storage.set(AuthConstants.CLIENT_REQUEST_TOKEN, token)
  }
}