import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '../../app/constants';
import { plainToClass } from 'class-transformer';
import { LoginResponse } from '../../models/login-reponse.model';
import { Storage } from '@ionic/storage';
import { User } from '../../models/user.model';
import { AuthConstants } from './auth.constants';
import { Trucker } from '../../models/trucker.model';


@Injectable()
export class AuthProvider {

  constructor(
    public http: HttpClient,
    private storage: Storage
  ) {
    
  }

  /**
   * Performs an authentication 
   * @param loginName
   * @param password 
   * @param mobileNumber 
   * 
   * @return Promise<LoginResponse>
   */
  login (loginName: string, password: string, mobileNumber: string = null): Promise<User> {

    let headers: HttpHeaders = new HttpHeaders({'Login-Token': 'TEST_LOGIN_TOKEN'});

    return new Promise((resolve, reject) => {

      const request = this.http.post(API.baseURL('login'), {
        LoginName: loginName,
        Password: password,
        MobileNo: mobileNumber,
        forDriver: true
      }, { headers });

      request.subscribe(res => {
        const response = plainToClass(LoginResponse, res['data'] as Object);
        this.storage.set(AuthConstants.USER_INFO, response.userInfo)
        this.storage.set(AuthConstants.TRUCKER_INFO, response.truckerInfo)
        resolve(response.userInfo)
      }, err => {
        reject(err) 
      })

    })
    
  }

  check(): Promise<User> {
    return new Promise((resolve, reject) => {
      this.storage.get(AuthConstants.USER_INFO)
        .then((user) => { 
          if(user instanceof Object){
            resolve(plainToClass(User, user as Object))
          }
          reject()
        })
        .catch(() => {
          reject()
        })
    })
  }

  getTruckerId(): Promise<number>{
    return new Promise((resolve, reject) => {
      this.storage.get(AuthConstants.TRUCKER_INFO)
        .then(trucker => {
          if(trucker instanceof Object){
            const info = plainToClass(Trucker, trucker as Object)
            resolve(info.Id)
          }
          reject()
        })
        .catch(() => {
          reject()
        })
    })
  }

}
