export class AuthConstants {
  static readonly CLIENT_REQUEST_TOKEN = 'NEXT_HTTP_REQUEST_TOKEN';
  static readonly SERVER_REQUEST_TOKEN = 'Token';
  static readonly USER_INFO = 'userInfo';
  static readonly TRUCKER_INFO = 'truckerInfo';

  static readonly DUMMY_SERVER_TOKEN_VALUE= 'TEST_TOKEN'
}