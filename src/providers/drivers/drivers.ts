import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '../../app/constants';
import { plainToClass } from 'class-transformer';
import { Driver } from '../../models/driver.model';

/*
  Generated class for the DriversProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DriversProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DriversProvider Provider');
  }

  getAllDrivers(truckerId: number) {
    let params: HttpParams = new HttpParams().set('truckerId', truckerId.toString());
    return this.http.get(API.baseURL('Drivers'), { params })
      .map(res => plainToClass(Driver, res['data'] as Object[]))
  }

}
