import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Truck } from '../../models/truck.model';
import { DriversProvider } from '../../providers/drivers/drivers';
import { AuthProvider } from '../../providers/auth/auth';
import { Driver } from '../../models/driver.model';

/**
 * Generated class for the SelectDriverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-driver',
  templateUrl: 'select-driver.html',
  providers: [AuthProvider]
})
export class SelectDriverPage {

  public truck: Truck;
  public drivers: Driver[]

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public driverService: DriversProvider,
    private auth: AuthProvider,
    public alertCtrl: AlertController
  ) {
    this.truck = this.navParams.get('truck')
    this.getDriversList()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectDriverPage');
  }

  async getDriversList() {
    const truckerId: number = <number> await this.auth.getTruckerId()
    this.driverService.getAllDrivers(truckerId)
      .subscribe(
        drivers => { 
          this.drivers = drivers 
          console.log(drivers)
        },
        err => { console.log(err) }
      )
  }

  dismiss() {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'You have not assigned a driver yet. Are you sure you want to exit from this page?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.viewCtrl.dismiss()
          }
        },
        {
          text: 'No',
          role: 'cancel',
        }
      ]
    });
    alert.present();
  }

}
